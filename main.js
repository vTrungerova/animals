function play(number) {
    let animal = document.getElementById('animal-' + number);
    let sound = document.getElementById('sound-' + number);
    animal.classList.add('hraje');
    sound.play();
}


function removeClass(number) {
    let animal = document.getElementById('animal-' + number);
    animal.classList.remove('hraje');
}


function keyDown(event) {
    const number = event.key;
    if (number !== null) {
        play(number);
    }
}

function keyUp(event) {
    const number = event.key;
    if (number !== null) {
        removeClass(number);
    }
}
